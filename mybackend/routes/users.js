const express = require('express');
const userController = require('../controller/UserController');
const router = express.Router();
const UserController = require('../controller/UserController');
/* GET users listing. */
router.get('/', function(req, res, next) {
  
  res.json(UserController.getUsers());
});

router.get('/', function(req, res, next) {
  const { id } = req.params
 res.json(userController.getUsers()) 
  
})
router.get('/:id', function(req, res, next) {
  const { id } = req.params
  res.json(userController.getUsers(id)) 
  
})
router.post('/', (req, res) => {
  const payload = req.body
 
  res.json(UserController.addUser(payload))
});


router.put('/', (req, res) => {
  const payload = req.body
 
  res.json(UserController.updateUser(payload))
})
router.delete('/:id', (req, res) => {
  const payload = req.body
 
  res.json(UserController.delelteUser(payload))
})


module.exports = router;
